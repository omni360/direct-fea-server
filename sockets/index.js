var OMC = require('./models/OpenSees/OpenSees_Model_Collection');


module.exports.init = function(sio) {
    var omc = new OMC({
        io : io
    });

    omc.sio = sio;
    sio.sockets.on('connection', function (socket) {
        var get_fe_model = function(data) {
            var mid = data;
                if (ut.isUndefined(mid)) {
                    mid = ut.uniqueId("fe_model_");
                }
            var model = self._models_map[mid];
            if (ut.isUndefined(self._models_map[mid])) {
                model = self.addModel(mid);
            }
            model.addClient(socket);
        };

        var interp_ops_cmd = function(str) {
            if(ut.isDefined(socket.model_id) && ut.isDefined(self._models_map[socket.model_id])) {
                self._models_map[socket.model_id]
                    .interpretOpenSeesCmd(str);
            }
        };
        
        var cud_api = function(operation) {
            if(ut.isDefined(socket.model_id) && ut.isDefined(self._models_map[socket.model_id])) {
                return self._models_map[socket.model_id].crud(operation);
            } else {
                return false;
            }
        };
        
        socket.on('fe-model-join', function(data) {
            var mid = data || "default";
            get_fe_model(mid);
        });
        socket.on('fe-model-create', get_fe_model);
        socket.on('fe-model-ops-interp', interp_ops_cmd);
        socket.on('fe-model-cud', interp_ops_cmd);
        
        // old version api
        socket.on('ops-create-interp', get_fe_model);
        socket.on('ops-switch-room', get_fe_model);
        socket.on('ops-stdin', interp_ops_cmd);
        socket.on('ops-stdin', function(str) {
            console.log('write to ops stdin:\n' + str);
        });
        socket.on('message', function(data) {
            console.log('message:', data);
        });
    });
};