var repl = require("repl");

var redis = require('redis');
var db = redis.createClient();
var pub = redis.createClient();
var sub = redis.createClient();

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var spawn = require('child_process').spawn;

var ut =require('util-lge');

var app = express();

app.configure(function(){
    app.set('port', process.env.PORT || 8080);
    app.set('opensees_exe', process.env.OPENSEES || 'myopensees');
    app.set('opensees_tcp_port', process.env.OPENSEES_TCP_PORT || 8124);
    app.set('gmsh_exe', process.env.GMSH || 'gmsh');
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(app.router);
    app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
    app.use(express.errorHandler());
});

var server = http.createServer(app);
var io = require('socket.io').listen(server);
server.listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});

var FEM = require('./models/Fe_Model_Manager');
var fem = new FEM({
    io : io
});
app.post('/fe_model', fem.ajaxAPI);

sub.subscribe("chat");

io.on("connection", function(client){
    client.send("welcome!");

    client.on("message", function(text){
        db.incr("messageNextId", function(e, id){
            db.hmset("messages:" + id, {
                uid : client.sessionId,
                text : text
            }, function(e, r) {
                pub.publish("chat", "messages:" + id);
            });
        });
    });

    client.on("disconnect", function(){
        client.broadcast(client.sessionId + " disconnected");
    });

    sub.on("message", function(pattern, key){
        store.hgetall(key, function(e, obj){
            client.send(obj.uid + ": " + obj.text);
        });
    });

});



    
var r = repl.start({
    prompt: ">> ",
    input: process.stdin,
    output: process.stdout
});
r.context.redis = redis;
r.context.db = db;
r.context.sub = sub;
r.context.pub = pub;
r.context.io = io;
r.context.ut = ut;
