puts " ---------------Start Analysis -----------------"; 
#Define the parameters that will change in different run cases in Opensees; 
foreach { 
AnalysisType   EQName  iGMfile   iGMdirection  iGMfact  iModel algorithmTypeDynamic} { 
Dynamic.EQ.bidirect CHICHI "TCU065-E TCU065-N" "1 2"  "1.46 1.46" Model_1  Newton  
} { 
wipe; 
source SIUnits.tcl; 
source 00_LibAnalysis.tcl; 
puts " -----------------------------------------"; 
puts "Model Set up and Gravity load analysis and EigenANA: starting"; 
set dataDir .;  
file mkdir  $dataDir; 
set dataDirModes $dataDir; 
file mkdir  $dataDirModes; 
set s1 [clock seconds]; 
source 11_ModelSetUp.tcl; 
set s2 [clock seconds]; 
set tout [expr $s2-$s1]; 
puts " -----------------------------------------"; 
puts "Model Built: $tout seconds"; 
puts " ----------------------------------------"; 
puts "$AnalysisType analysis: starting..."; 
source 30_LibAnalysisDynamicParameters.tcl; 
source 31_Analyze.EQ.TimeHistory.tcl; 
puts "$AnalysisType analysis: complete..."; 
set s4 [clock seconds]; 
set tout [expr $s4-$s2]; 
puts "$AnalysisType analysis: $tout seconds"; 
puts " -----------------------------------------"; 
puts " --------------- End Analysis -----------------"; 
}; 
