# Eigen Analysis.
puts "Yong: staring Eigen Analysis in 13_EigenAnalysis.tcl"
file mkdir modes; #Directory to store data for eigen analysis.
set numModes 10;

# for { set k 1 } { $k <= $numModes } { incr k } {
    # recorder Node -file [format "modes/mode%i.out" $k] -nodeRange 401 407 -dof 1 2 3  "eigen $k";
# } 
# recorder Node -file mode1.out -nodeRange 401 407 -dof 1 2 3  "eigen 1";

# -genBandArpack, -symmSparseArpack, -symmBandLapack, -fullGenLapack, -UmfPack, -SuperLU 
set lambda [eigen -generalized -genBandArpack $numModes]; 
# #############  NOTE for the MargaMarga Bridge ###################
# ################################################################## 

set omega [list ];
set F [list ];
set T [list ];
set PI [expr 2.0*asin(1.0)];

foreach lam $lambda {
	#puts "$lam";
	#puts "[expr sqrt($lam)]"; # Why negative values?
	lappend omega [expr sqrt($lam)];
	lappend F [expr sqrt($lam)/(2*$PI)];
	lappend T [expr (2*$PI)/sqrt($lam)];
}

# Write out the Periods.
set period "modes/Periods.out"
set fileID [open $period "w"];
set temp_i 0;
foreach t $T {
	incr temp_i;
	puts "period of mode: $temp_i $t sec"; # Print Period to screen.
    puts $fileID " $t";
}
close $fileID;

# Write out the Frequencies.
set Frequencies "modes/Frequencies.out"
set fileID [open $Frequencies "w"];
set temp_i 0;
foreach f $F {
	incr temp_i;
	puts "Frequency of mode: $temp_i $f Hz"; # Print Period to screen.
    puts $fileID " $f";
}
close $fileID;


source eigenvectorRecorder.tcl 