set dirKeyword "Data_3D_Model01_E*"
set dirList [glob -type d $dirKeyword]
set list1 [split $dirList {" "}]
foreach item $list1 {
     cd $item
     set xmlFileList [glob *.xml]
     set list2 [split $xmlFileList {" "}]
     foreach xmlFile $list2 {
         set txtFile [string map {xml txt} $xmlFile]
         stripXML $xmlFile $txtFile
     }
     cd ..
}
