proc AnalyzeTimeHistory {args} {
	#########################################################
	#  AnalyzeTimeHistory 
	##  or
	## AnalyzeTimeHistory -TmaxAnalysis $TmaxAnalysis  -analysisType $analysisType -constraintsType $constraintsType -alphaSP $alphaSP -alphaMP $alphaMP \
	##			-numbererType $numbererType -systemType $systemType -testType $testType -maxNumIter $maxNumIter printFlag $printFlag -NstepReduceIncrement $NstepReduceIncrement \
	## 			-maxNumIterConverge $maxNumIterConverge -printFlagConverge $printFlagConverge -algorithmType $algorithmType -NewtonLineSearchRatio $NewtonLineSearchRatio \
	## 			-algorithmCount $algorithmCount -Tolerance $Tolerance -integratorType $integratorType -NewmarkGamma $NewmarkGamma -NewmarkBeta $NewmarkBeta -DtAnalysis $DtAnalysis 
	##  or
	## set AnalysisData "TmaxAnalysis $TmaxAnalysis analysisType $analysisType constraintsType $constraintsType alphaSP $alphaSP alphaMP $alphaMP \
	##			numbererType $numbererType systemType $systemType testType $testType maxNumIter $maxNumIter printFlag $printFlag NstepReduceIncrement $NstepReduceIncrement \
	## 			maxNumIterConverge $maxNumIterConverge printFlagConverge $printFlagConverge algorithmType $algorithmType NewtonLineSearchRatio $NewtonLineSearchRatio \
	## 			algorithmCount $algorithmCount Tolerance $Tolerance integratorType $integratorType NewmarkGamma $NewmarkGamma NewmarkBeta $NewmarkBeta DtAnalysis $DtAnalysis 
	## AnalyzeTimeHistory  -AnalysisData $AnalysisData
	#########################################################
	# perform dynamic (Transient) ground-motion analysis based on previously-defined load pattern
	#	   Silvia Mazzoni, 2006 (mazzoni@berkeley_NO_SPAM_.edu)
	### set analysis parameters

	set DefaultTimeHistoryAnalysisModelData  "TmaxAnalysis 50. analysisType Transient constraintsType Transformation alphaSP 1e6 alphaMP 1e6 numbererType RCM \
			systemType BandGeneral testType NormDispIncr  maxNumIter 100 printFlag 2 NstepReduceIncrement 4 maxNumIterConverge 2000 \
			printFlagConverge 2 algorithmType Newton NewtonLineSearchRatio 0.8 algorithmCount 5 Tolerance 1e-5  integratorType Newmark NewmarkGamma 0.5 NewmarkBeta 0.25 DtAnalysis 0.01 "
	foreach {Name DefaultValue} $DefaultTimeHistoryAnalysisModelData {
		set $Name $DefaultValue
	}
	# NOTE: check for rigid diaphragms. if they are used, you should use Lagrange Constraints. Transformation is good for large models.

	# override defaults with anything specified in argument list:
	foreach {Option Value} $args {
		if {[string index $Option 0]=="-"} {;							# remove the hyphen
			set Option [string range $Option 1 end]
		}
		set $Option $Value
		if {[llength $Value]>1} {;											# if the argument is a list
			foreach {SubName SubValue} $Value {
				if {[string index $SubName 0]=="-"} {;				# remove the hyphen
					set SubName [string range $SubName 1 end]
				}
				set $SubName $SubValue
			}
		}
	}

# -------------------------------------------------- clean analysis model and results -----------------------
	wipeAnalysis
# ---------------------------------------------------------------------------------------------------------------------
	set TmaxAnalysis [expr 1.0*$TmaxAnalysis]
	
	if {$constraintsType == "Plain" | $constraintsType == "Transformation"} {;	# CONSTRAINTS
	   constraints $constraintsType ;     # Plain & Transformation constraints
	} else {
	   constraints $constraintsType $alphaSP $alphaMP ;     # Penalty & Lagrange
	}

	if {$systemType  == "SparseGeneralPivot"} {;					# SYSTEM
	   system SparseGeneral  -piv ;	# optional pivoting for SparseGeneral system
	} else {
	   system $systemType ; 
	}

   	numberer $numbererType;								# NUMBERER
   	test $testType $Tolerance $maxNumIter $printFlagConverge;		# TEST

	if {$algorithmType == "BFGS" | $algorithmType == "Broyden" } {;		# ALGORITHM
		algorithm $algorithmType $algorithmCount;	# use Newton's solution algorithm: updates tangent stiffness at every iteration
	} elseif  {$algorithmType == "NewtonLineSearch"} {
		algorithm $algorithmType $NewtonLineSearchRatio;	
	} else {
		algorithm $algorithmType;	
	}

   	if {$integratorType =="Newmark"} {;						# INTEGRATOR
		   integrator $integratorType $NewmarkGamma $NewmarkBeta   
	} else {
   		   integrator $integratorType $NewmarkGamma
	}

	analysis $analysisType;							# ANALYSIS
	
# -------------------------------------------------------------------------------------------
	set ok 0;
	set controlTime [getTime];
	
	while {$controlTime < $TmaxAnalysis && $ok == 0} {
		set controlTime [getTime]
		set Nsteps [format %.0f [expr ($TmaxAnalysis-$controlTime)/$DtAnalysis]]
		if {$Nsteps >10} {set Nsteps 10}
		# ----------------------------------------------check if GUI has tried to stop the analysis------------------------
		global StopAnalysisSwitch
		update
		if {[info exist StopAnalysisSwitch]} {
			if {$StopAnalysisSwitch == "yes"} {return}
		}
		# ----------------------------------------------first analyze command------------------------
		set ok [analyze $Nsteps $DtAnalysis]
		# ----------------------------------------------if convergence failure-------------------------
		if {$ok != 0} {;								# performance is slower inside this loop
			puts "Trying Reducing This Time Step by 2 .."
			set ok [analyze 2 [expr $DtAnalysis/2]]
			if {$ok != 0} {
				puts "Trying Reducing This Time Step by 10 .."
				set ok [analyze 10 [expr $DtAnalysis/10]]
			}
			if {$ok != 0} {
				puts "Trying Reducing This Time Step by 100 .."
				set ok [analyze 100 [expr $DtAnalysis/100]]
				test $testType $Tolerance $maxNumIter  $printFlagConverge
				algorithm $algorithmType
			}
			if {$ok != 0} {
				puts "Trying Newton with Initial Tangent .."
				test $testType $Tolerance $maxNumIterConverge $printFlagConverge
				algorithm Newton -initial
				set ok [analyze 1 $DtAnalysis]
				test $testType $Tolerance $maxNumIter  $printFlag
				algorithm $algorithmType
			}
			if {$ok != 0} {
				puts "Trying Broyden .."
				algorithm Broyden 8
				set ok [analyze 1 $DtAnalysis]
				algorithm $algorithmType
			}
			if {$ok != 0} {
				puts "Trying NewtonWithLineSearch .."
				algorithm NewtonLineSearch $NewtonLineSearchRatio
				set ok [analyze 1 $DtAnalysis]
				algorithm $algorithmType
			}
			if {$ok == 0} {puts "Converged at this step, continuing...."}
		}
		# -----------------------------------------------------------------------------------------------------
		catch {DisplayAnalysisDisplays}		;			# run this proc only if it has been defined, continue w/o error otherwise
	}
	# final confirmation to see if we made it past the problems:
	set fmt1 "%s analysis: endTime=%.2f."
	if {$ok != 0 } {
		set AnalysisResults [format $fmt1 "PROBLEM" [getTime]]
	} else {
		set AnalysisResults [format $fmt1 "DONE"  [getTime]]
	}
# 	puts $AnalysisResults;	# print results to screen
	return 0
}						