proc ReadPEERNGAFile {inFilename outFilename dt npts} {
	#########################################################
	# ReadPEERNGAFile   $inFilename $outFilename $dt $npts	
	#########################################################
	# read gm input format and output to opensees file
	#
	# Written: MHS
	# Date: July 2000
	# modified for new PEERNGA file by Andre Barbosa, 2010
	#
	# A procedure which parses a ground motion record from the PEER
	# strong motion database by finding dt in the record header, then
	# echoing data values to the output file.
	#
	# Formal arguments
	# inFilename -- file which contains PEER strong motion record
	# outFilename -- file to be written in format G3 can read
	# dt -- time step determined from file header
	# npts -- time step determined from file header
	# Assumptions
	#	The header in the PEER record is, e.g., formatted as follows:
	#	 PEER NGA STRONG MOTION DATABASE RECORD
	#	 NORTHRIDGE AFTERSHOCK 01/17/94,ANAVERDE VALLEY - CITY RANCH, 090                
	#	 ACCELERATION TIME HISTORY IN UNITS OF G
	#	 4001    0.0100    NPTS, DT
	
	upvar $dt DT;		# Pass dt by reference
        upvar $npts NPTS
        
	# READ Load Counter
	# Open the input file and catch the error if it can't be read, set LoadCounter to 1000
	if [catch {open $inFilename r} inFileID] {
		set LoadCounter 1000
	} else {
		# Open output file for writing
		set outFileID [open $outFilename w]

		# Flag indicating dt is found and that ground motion
		# values should be read -- ASSUMES dt is on last line
		# of header!!!
		set flag 0
		# Look at each line in the file
		foreach line [split [read $inFileID] \n] {
			if {[llength $line] == 0} {
				# Blank line --> do nothing
				continue
			} elseif {$flag == 1} {
				# Echo ground motion values to output file
				puts $outFileID $line
			} else {
				# Search header lines for dt
				foreach word [split $line] {
					# Read in the time step
					# Find the desired token and set the flag
					if {[string match $word "DT"] == 1} {
						set NPTS [lindex $line 0]
						set DT [lindex $line 1]
						set flag 1
					}
				}
			}
		}
		close $outFileID;	# Close the output file
		close $inFileID;	# Close the input file
	}
}