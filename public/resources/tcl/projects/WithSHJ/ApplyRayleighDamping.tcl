proc ApplyRayleighDamping {DampingRatio args} {
	#########################################################
	#  ApplyRayleighDamping $DampingRatio 
	##  or
	## ApplyRayleighDamping $DampingRatio -MproportionalSwitch 0. -KcurrentSwitch 0. -KcommittedSwitch 0. -KinitialSwitch 1. -iEigen 1 -jEigen 3
	##  or
	## set DampingData "MproportionalSwitch 0. KcurrentSwitch 0. KcommittedSwitch 0. KinitialSwitch 1. iEigen 1 jEigen 3"
	## ApplyRayleighDamping $DampingRatio -DampingData $DampingData
	#########################################################
	# apply rayleigh damping
	#	   Silvia Mazzoni, 2006 (mazzoni@berkeley_NO_SPAM_.edu)
	### set analysis parameters
	##   DampingRatio;		# damping ratio

	set DefaultDampingData  "MproportionalSwitch 0. KcurrentSwitch 0. KcommittedSwitch 0. KinitialSwitch 1. iEigen 1 jEigen 3"
	set DefaultDampingData  "MproportionalSwitch 19. KcurrentSwitch 18. KcommittedSwitch 10. KinitialSwitch 11. iEigen 11 jEigen 13"
	foreach {Name DefaultValue} $DefaultDampingData {
		set $Name $DefaultValue
	}

	# override defaults with anything specified in argument list:
	foreach {Option Value} $args {
		if {[string index $Option 0]=="-"} {;							# remove the hyphen
			set Option [string range $Option 1 end]
		}
		set $Option $Value
		if {[llength $Value]>1} {;											# if the argument is a list
			foreach {SubName SubValue} $Value {
				if {[string index $SubName 0]=="-"} {;				# remove the hyphen
					set SubName [string range $SubName 1 end]
				}
				set $SubName $SubValue
			}
		}
	}
# -------------------------------------------------- apply damping -----------------------
	set ok [eigen [expr $jEigen]];			# eigenvalue analysis for jEigen modes
	if {$ok>0} {;				# MDOF
		set lambdaN $ok
		set lambdaI [lindex $lambdaN [expr $iEigen-1]]; 		# eigenvalue mode i
		set lambdaJ [lindex $lambdaN [expr $jEigen-1]]; 	# eigenvalue mode j
	} else {;					# SDOF
		set lambdaN [eigen 1]
		set lambdaI [lindex $lambdaN [expr $iEigen-1]]; 		# eigenvalue mode i
		set lambdaJ 0.0;
	}
	set omegaI [expr pow($lambdaI,0.5)];
	set omegaJ [expr pow($lambdaJ,0.5)];
	set alphaM [expr $MproportionalSwitch*$DampingRatio*(2*$omegaI*$omegaJ)/($omegaI+$omegaJ)];	# M-proportional. damping; D = alphaM*M
	set betaKcurrent [expr $KcurrentSwitch*2.*$DampingRatio/($omegaI+$omegaJ)];         		# K-proportional damping;      +beatKcurrent*KCurrent
	set betaKcommmitted [expr $KcommittedSwitch*2.*$DampingRatio/($omegaI+$omegaJ)];   		# K-proportional. damping parameter;   +betaKcommmitted*KlastCommitt
	set betaKinitial [expr $KinitialSwitch*2.*$DampingRatio/($omegaI+$omegaJ)];         			# initialial-stiffness proportional damping      +beatKinitial*Kini

	rayleigh $alphaM $betaKcurrent $betaKinitial $betaKcommmitted; 				# apply RAYLEIGH damping
	return 0
}						
