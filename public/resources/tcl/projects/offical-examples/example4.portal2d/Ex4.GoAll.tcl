
puts " ---------------Elastic Model --------------------"
puts " ---------------Static Pushover Analysis -----------------"
source Ex4.Portal2D.build.ElasticElement.tcl
source Ex4.Portal2D.analyze.Static.Push.tcl

puts " ---------------Elastic Model --------------------"
puts " ---------------Static Reversed-Cyclic Analysis -----------------"
source Ex4.Portal2D.build.ElasticElement.tcl
source Ex4.Portal2D.analyze.Static.Cycle.tcl

puts " ---------------Elastic Model --------------------"
puts " ---------------Uniform Sine-wave Excitation -----------------"
source Ex4.Portal2D.build.ElasticElement.tcl
source Ex4.Portal2D.analyze.Dynamic.sine.Uniform.tcl

puts " ---------------Elastic Model --------------------"
puts " ---------------Uniform Earthquake Excitation -----------------"
source Ex4.Portal2D.build.ElasticElement.tcl
source Ex4.Portal2D.analyze.Dynamic.EQ.Uniform.tcl

puts " ---------------Elastic Model --------------------"
puts " ---------------Multiple-support Sine-wave Excitation -----------------"
source Ex4.Portal2D.build.ElasticElement.tcl
source Ex4.Portal2D.analyze.Dynamic.sine.multipleSupport.tcl

puts " ---------------Elastic Model --------------------"
puts " ---------------Multiple-support Earthquake Excitation -----------------"
source Ex4.Portal2D.build.ElasticElement.tcl
source Ex4.Portal2D.analyze.Dynamic.EQ.multipleSupport.tcl


puts " ---------------Elastic Model --------------------"
puts " --  Bidirectional Earthquake Excitation --"
source Ex4.Portal2D.build.ElasticElement.tcl
source Ex4.Portal2D.analyze.Dynamic.EQ.bidirect.tcl



puts " ----------------------------------------------"

puts " ---------------Uniaxial Inelastic Section, Nonlinear Model --------------------"
puts " ---------------Static Pushover Analysis -----------------"
source Ex4.Portal2D.build.InelasticSection.tcl
source Ex4.Portal2D.analyze.Static.Push.tcl

puts " ---------------Uniaxial Inelastic Section, Nonlinear Model --------------------"
puts " ---------------Static Reversed-Cyclic Analysis -----------------"
source Ex4.Portal2D.build.InelasticSection.tcl
source Ex4.Portal2D.analyze.Static.Cycle.tcl

puts " ---------------Uniaxial Inelastic Section, Nonlinear Model --------------------"
puts " ---------------Uniform Sine-wave Excitation -----------------"
source Ex4.Portal2D.build.InelasticSection.tcl
source Ex4.Portal2D.analyze.Dynamic.sine.Uniform.tcl

puts " ---------------Uniaxial Inelastic Section, Nonlinear Model --------------------"
puts " ---------------Uniform Earthquake Excitation -----------------"
source Ex4.Portal2D.build.InelasticSection.tcl
source Ex4.Portal2D.analyze.Dynamic.EQ.Uniform.tcl


puts " ---------------Uniaxial Inelastic Section, Nonlinear Model --------------------"
puts " ---------------Multiple-support Sine-wave Excitation -----------------"
source Ex4.Portal2D.build.InelasticSection.tcl
source Ex4.Portal2D.analyze.Dynamic.sine.multipleSupport.tcl

puts " ---------------Uniaxial Inelastic Section, Nonlinear Model --------------------"
puts " ---------------Multiple-support Earthquake Excitation -----------------"
source Ex4.Portal2D.build.InelasticSection.tcl
source Ex4.Portal2D.analyze.Dynamic.EQ.multipleSupport.tcl

puts " ---------------Uniaxial Inelastic Section, Nonlinear Model --------------------"
puts " --  Bidirectional Earthquake Excitation --"
source Ex4.Portal2D.build.InelasticSection.tcl
source Ex4.Portal2D.analyze.Dynamic.EQ.bidirect.tcl



puts " ----------------------------------------------"

puts " ---------------Uniaxial Inelastic Material, Fiber Section, Nonlinear Model --------------------"
puts " ---------------Static Pushover Analysis -----------------"
source Ex4.Portal2D.build.InelasticFiberSection.tcl
source Ex4.Portal2D.analyze.Static.Push.tcl

puts " ---------------Uniaxial Inelastic Material, Fiber Section, Nonlinear Model --------------------"
puts " ---------------Static Reversed-Cyclic Analysis -----------------"
source Ex4.Portal2D.build.InelasticFiberSection.tcl
source Ex4.Portal2D.analyze.Static.Cycle.tcl

puts " ---------------Uniaxial Inelastic Material, Fiber Section, Nonlinear Model --------------------"
puts " ---------------Uniform Sine-wave Excitation -----------------"
source Ex4.Portal2D.build.InelasticFiberSection.tcl
source Ex4.Portal2D.analyze.Dynamic.sine.Uniform.tcl

puts " ---------------Uniaxial Inelastic Material, Fiber Section, Nonlinear Model --------------------"
puts " ---------------Uniform Earthquake Excitation -----------------"
source Ex4.Portal2D.build.InelasticFiberSection.tcl
source Ex4.Portal2D.analyze.Dynamic.EQ.Uniform.tcl

puts " ---------------Uniaxial Inelastic Material, Fiber Section, Nonlinear Model --------------------"
puts " ---------------Multiple-support Sine-wave Excitation -----------------"
source Ex4.Portal2D.build.InelasticFiberSection.tcl
source Ex4.Portal2D.analyze.Dynamic.sine.multipleSupport.tcl

puts " ---------------Uniaxial Inelastic Material, Fiber Section, Nonlinear Model --------------------"
puts " ---------------Multiple-support Earthquake Excitation -----------------"
source Ex4.Portal2D.build.InelasticFiberSection.tcl
source Ex4.Portal2D.analyze.Dynamic.EQ.multipleSupport.tcl

puts " ---------------Uniaxial Inelastic Section, Nonlinear Model --------------------"
puts " --  Bidirectional Earthquake Excitation --"
source Ex4.Portal2D.build.InelasticSection.tcl
source Ex4.Portal2D.analyze.Dynamic.EQ.bidirect.tcl

