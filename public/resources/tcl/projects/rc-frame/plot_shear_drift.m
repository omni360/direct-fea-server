close all
dirs={'force_1_5_p0' 'force_1_5_p1' 'force_1_5_p2'};
ele_conf='force-based element';
lstr={'p/(Ag*fc)=0.0','p/(Ag*fc)=0.1','p/(Ag*fc)=0.2'};
Lcol=4;
f1=figure();
f2=figure();
markers={'-r' '--k' '-.b'};
for i=[1 2 3]
    temp=load([dirs{i} '/base_shear.out']);
    
    H=-sum(temp,2)/3;
    
    U1=load([dirs{i} '/floor1_dispx.out']);
    U2=load([dirs{i} '/floor2_dispx.out']);
    
    figure(f1);
    hold on;
    plot(U1/Lcol,3*H/1e3,markers{i});
    
    
    figure(f2);
    hold on;
    plot((U2-U1)/Lcol,2*H/1e3,markers{i});
   
end

figure(f1);
title(sprintf(['Total story shear VS interstory drift (first story)\n',ele_conf]));
xlabel('Interstory drift ratio');
ylabel('Total story shear (kN)');
grid on;
legend(lstr,'Location','SouthEast');

figure(f2);
title(sprintf(['Total story shear VS interstory drift (second story)\n',ele_conf]));
xlabel('Interstory drift ratio');
ylabel('Total story shear (kN)');
grid on;
legend(lstr,'Location','SouthEast');


