model basic -ndm 2 -ndf 3
geomTransf Linear 1;
geomTransf PDelta 2;
geomTransf Corotational 3;
node 1 600 240 -mass 2428.2 2428.2 2428.2;
node 2 600 120 -mass 2683.8 2683.8 2683.8;
node 3 600 240 -mass 1704 1704 1704;
element elasticBeamColumn 1 1 3 100 29000 833.3333 3;
element elasticBeamColumn 2 3 2 100 29000 833.3333 3;
fix 2 1 1 1;
timeSeries Constant 1 -factor 1;
pattern Plain 1 1 {;
load 3 120 -20 0;
};
constraints Plain;
numberer Plain;
system BandGeneral;
test NormDispIncr 1e-8 15;
algorithm Newton;
integrator LoadControl 1;
analysis Static;
analyze 1;
puts [allNodeDisp];
