close all
dirs={'disp_2_4'};
ele_conf={'2 displacement-based elements'};
lstr={'from element end force','from section response'};
floStr={'floor1' 'floor2'};
floStr2={'first floor' 'second floor'};
Lcol=4;
Ystart=[0 Lcol];
nele_arr=[2 4 6];
nsec_arr=[4 4 4];
for i=1
    nele=nele_arr(i);
    nsec=nsec_arr(i);
    
    for flo=[1 2]
        
        
        temp=load([dirs{i} '/' floStr{flo} '_left_col_endforce.out']);
        temp=temp(end,:);
        temp2=load([dirs{i} '/' floStr{flo} '_left_col_sectionforce.out']);
        temp2=temp2(end,:);
        m1=zeros(nele*2,1);
        y1=zeros(nele*2,1);
        m2=zeros(nele*nsec,1);
        y2=zeros(nele*nsec,1);
        dY1=Lcol/nele;
        Y1=Ystart(flo);
        Y2=(-lglnodes(nsec-1)+1)/2*dY1;
        for j=1:nele
            m1(2*j-1)=-temp(6*j-3);
            m1(2*j)=temp(6*j);
            y1(2*j-1)=Y1;

            y2((j-1)*nsec+1:j*nsec)=Y1+Y2;


            Y1=Y1+dY1;
            y1(2*j)=Y1;

            for k=1:nsec
                m2(nsec*(j-1)+k)=temp2((j-1)*2*nsec+(k-1)*2+2);          
            end


        end

        figure;
        plot(m1,y1,'-o',m2,y2,'-*');
        title(sprintf(['Moment diagram for left column in ' floStr2{flo} '\n' ele_conf{i}]))
        xlabel('Moment (N*m)');
        ylabel('Height (m)');
        legend(lstr,'Location','NorthWest');
        grid on;
    end
    
    
    
   
end