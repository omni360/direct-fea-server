% plotter

clc, close all

for i=7;
    
    fold=['config_' num2str(i) '/'];
    
    data=dlmread([fold 'eigenvalues.txt']);
    for i=1:length(data)
        period(i,:)=2*pi/sqrt(data(i));
    end
    
    data=dlmread([fold 'eigen1.txt']);
    eigen1=data(1,2:end);
    eigen1=eigen1/max(abs(eigen1));
    data=dlmread([fold 'eigen2.txt']);
    eigen2=data(1,2:end);
    eigen2=eigen2/max(abs(eigen2));
    data=dlmread([fold 'eigen3.txt']);
    eigen3=data(1,2:end);
    eigen3=eigen3/max(abs(eigen3));
    
    mass=[2110,2110,2283];
    
    mp1=sum(mass.*eigen1)^2./sum(mass.*eigen1.^2)/sum(mass);
    mp2=sum(mass.*eigen2)^2./sum(mass.*eigen2.^2)/sum(mass);
    mp3=sum(mass.*eigen3)^2./sum(mass.*eigen3.^2)/sum(mass);
    MP=[mp1;mp2;mp3]
    
    
    
    for i=1:length(data)
        mp(i,:)=2*pi/sqrt(data(i));
    end
    
    data=dlmread([fold 'roof_disp.txt']);
    data2=dlmread([fold 'Yreacts.txt']);
    weight=323.82;
    force=1;
    height=12*39;
    hold on
    plot(data(:,2)/height*100,data(:,1)*force/weight,'b-')
    xlabel(' Drift (%) ')
    ylabel(' Seismic Coefficient (g)')
    period
    
    output=[data(:,2)/height,data(:,1)*force/weight];
    
    data=dlmread([fold 'is_drift.txt']);
    is=data;
    heights=[18,18,18]*12;
    temp=data(end,2:end);
    temp2=[];
    for i=2:1:length(temp)
        temp2=[temp2,temp(i)-temp(i-1)];
    end
    is_drift=temp2./height;
    
    data=dlmread([fold 'pW_disp.txt']);
    heights=[18,18,18]*12;
    temp=data(end,2:end);
    temp2=[];
    for i=2:2:length(temp)
        temp2=[temp2,temp(i)-temp(i-1)];
    end
    pw_drift=temp2./height;
    
    data=dlmread([fold 'pw_forces.txt']);
    temp=data(end,2:end);
    
    
end
