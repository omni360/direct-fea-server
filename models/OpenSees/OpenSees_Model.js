/** 
 * @author: Li Ge, lge@ucsd.edu
 * Module Name: OpenSees_Model.js
 * Description: 
 * Configure Options: 
 * Properties: 
 * Methods: 
 * Events: 
*/

var Backbone = require('backbone');
// var store = require('redis').createClient();
var ut = require('util-lge');
var spawn = require('child_process').spawn;
var exec = require('child_process').exec;
// var path = require('path');

var OpenSees_Model = Backbone.Model.extend({
    defaults : function() {
        return {
            createdTime : new Date(),
            interactive : true,
            owner : "server",
            opensees_exe : "myopensees",
            opensees_args : "--no-prompt",
            opensees_tcp_port : 8124,
            opensees_scripts_path : __dirname
                + "/../../public/resources/tcl/"
        };
    },
    validate : function(attrs) {
        
    },
    initialize : function(data) {

        // debugger;
        ut.bindAll(this);
        // console.log('this:', this);
        // this.createdTime = new Date();
        if (this.get('interactive')) {
            this.createOpenSeesInterp();
        }
        
        this.clients = {};

        // gmsh objects:
        this.geo_elementaries = {
            points : {},
            lines : {},
            line_loops : {},
            surfaces : {},
            surface_loops : {},
            volumns : {}
        };
        
        this.groups = {
            physical_points : {},
            physical_lines : {},
            physical_surfaces : {},
            physical_volumns : {}
        };

        // 
        this.geo_transforms = {};
        this.sections = {};
        this.uniaxial_materials = {};
        this.nd_materials = {};
        this.time_series = {};
        this.patterns = {};

        this.msh = {
            elements : {},
            nodes : {}
        };
    },
    createOpenSeesInterp : function() {
        var model = this;
        var self = this;
        var interp = spawn(
            this.get('opensees_exe'),
            this.get('opensees_args').split(' ')
        );
        interp.model_id = this.id;

        // init scripts
        
        interp.stdin.write(
            'set TCL_ROOT '
                + this.get('opensees_scripts_path') + ';'
                + 'cd $TCL_ROOT;'
                + 'json-set-tcp 127.0.0.1' + ' '
                + this.get('opensees_tcp_port') + ' '
                + this.id + ';'
                + '\n'
        );

        interp.stdout.on('data', function(data) {           
            if (self.sio) {
                self.sio.sockets.in(model.id)
                    .emit('ops-stdout', data.toString());
            } else {
                console.log('ops-stdout', data.toString());
            }
        });
        
        interp.stderr.on('data', function(data) {           
            if (self.sio) {
                self.sio.sockets.in(model.id)
                    .emit('ops-stderr', data.toString());
            } else {
                console.log('ops-stderr', data.toString());
            }
        });
        
        interp.on('exit', function(code, sig) {
            console.log('interp with pid ' + interp.pid +
                        ' exits, code: ' + code + ' signal: ' + sig);
            var clients = model.clients;
            if (ut.size(clients) === 0 && model.name !== 'default') {
                model.collection.remove(model);
            }
            if (self.sio) {
                self.sio.sockets.in(model.id)
                    .emit('ops-server-closed', {
                        code : code,
                        signal : sig
                    });
            }
          
        });
        this.opensees_interp = interp;
    },
    getFeInterp : function() {
        return this.opensees_interp;
    },

    listClients : function() {
        return ut.keys(this.clients);
    },

    addClient : function(socket) {
        var self = this;
        this.clients[socket.id] = socket;
        if (ut.isDefined(socket.model_id)) {
            socket.leave(socket.model_id);
        }
        socket.model_id = this.id;
        socket.model = this;
        socket.join(socket.model_id);
        socket.on('disconnect', function() {
            delete self.clients[socket.id];
        });
    },

    interpretOpenSeesCmd : function(str) {   
        function formatCommand(str) {
            var cmds=[];
            var cmd;
            if (!str) {
                return '';
            } else {
                var result = '';
                str.trim().split(/[\n;]/).forEach(function(s) {
                    var cmd = s.trim().replace(/\s+/g, ' ');
                    if (cmd !== '' && cmd[0] !== '#') {
                        cmds.push(cmd);
                    }
                });
                for (var i = -1, len = cmds.length; ++i < len;) {
                    cmd = cmds[i].trim();
                    result += cmd + ';';
                }
                result += "\n";
                return result;
            }
        }
        
        if (ut.isDefined(this.opensees_interp)) {
            this.opensees_interp.stdin.write(formatCommand(str));
            return true;
        } else {
            return false;
        }
    }
    
});

module.exports = OpenSees_Model;