/** 
 * @author: Li Ge, lge@ucsd.edu
 * Module Name: Fe_Model_Manager.js
 * Description: 
 * Configure Options: 
 * Properties: 
 * Methods: 
 * Events: 
*/

var ut = require('util-lge');
var net = require('net');
var Fe_Model = require("./OpenSees_Model.js");

var Backbone = require('backbone');

var Fe_Model_Collection = Backbone.Collection.extend({
    model : Fe_Model,
    defaults : {
        opensees_tcp_port : 8124
    },
    initialize: function(initData) {
        debugger;
        var self = this;
        this._tcp_server = net.createServer(function(c) {
            console.log('server connected');
            c.on('end', function() {
                console.log('server disconnected');
            });
            c.on('data', function(data) {
                if (typeof c.id === 'undefined') {
                    c.id = data.toString();
                } else {
                    self._io.sockets.in(c.id)
                        .emit('ops-json-domain', data.toString()); 
                }
            });
        });

        this._tcp_server.listen(
            self.defaults['opensees_tcp_port'],
            function() {
                console.log('tcp server listen port '
                            + self.defaults['opensees_tcp_port']);
            }
        );
        
        if (initData && initData.io) {
            this.bindSocketIO(initData.io);
        }

        // this.on('add', this.onAdd, this);
        // this.on('remove', this.onRemove, this);
        // this.on('change', this.onChange, this);
    },
    
    onAdd : function(model) {
        // model.sio = self.sio;
    },

    onRemove : function(model) {
        
    },
    onChange : function(model) {
        
    }
    
    // bindSocketIO : function(sio) {
    //     var self = this;
    //     this.sio = sio;
    //     sio.sockets.on('connection', function (socket) {
    //         var get_fe_model = function(data) {
    //             var mid = data;
    //             if (ut.isUndefined(mid)) {
    //                 mid = ut.uniqueId("fe_model_");
    //             }
    //             var model = self._models_map[mid];
    //             if (ut.isUndefined(self._models_map[mid])) {
    //                 model = self.addModel(mid);
    //             }
    //                 model.addClient(socket);
    //         };

    //         var interp_ops_cmd = function(str) {
    //             if(ut.isDefined(socket.model_id) && ut.isDefined(self._models_map[socket.model_id])) {
    //                 self._models_map[socket.model_id]
    //                         .interpretOpenSeesCmd(str);
    //             }
    //         };
            
    //         var cud_api = function(operation) {
    //             if(ut.isDefined(socket.model_id) && ut.isDefined(self._models_map[socket.model_id])) {
    //                 return self._models_map[socket.model_id].crud(operation);
    //             } else {
    //                     return false;
    //             }
    //         };
            
    //         socket.on('fe-model-join', function(data) {
    //             var mid = data || "default";
    //             get_fe_model(mid);
    //         });
    //         socket.on('fe-model-create', get_fe_model);
    //         socket.on('fe-model-ops-interp', interp_ops_cmd);
    //         socket.on('fe-model-cud', interp_ops_cmd);
            
    //         // old version api
    //         socket.on('ops-create-interp', get_fe_model);
    //         socket.on('ops-switch-room', get_fe_model);
    //         socket.on('ops-stdin', interp_ops_cmd);
    //         socket.on('ops-stdin', function(str) {
    //             console.log('write to ops stdin:\n' + str);
    //             });
    //         socket.on('message', function(data) {
    //             console.log('message:', data);
    //         });
    //     });
    // },

    // ajaxAPI : function(req, res) {
    //         var sid = req.query.socket_id;
    //     var sc = this._io.sockets.socket(sid);
    //     var interp = this.getFeInterp();
    //     if (ut.isDefined(interp)) {
    //         interp.stdin.write(formatCommand(req.query.commands));
    //         res.jsonp({
    //             sid : sid ,
    //             room : sc.ops_room_id
    //         });
    //     } else {
    //         res.jsonp(500, {
    //             sid : sid ,
    //             room : sc.ops_room_id,
    //             error : true
    //             });
    //         }
    // }


    
});

module.exports = Fe_Model_Collection;