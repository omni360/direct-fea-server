/** 
 * @author: Li Ge, lge@ucsd.edu
 * Module Name: Fe_Model.js
 * Description: 
 * Configure Options: 
 * Properties: 
 * Methods: 
 * Events: 
*/

var ut = require('util-lge');
var spawn = require('child_process').spawn;

var defaults = {
    "id" : "default",
    "owner" : "server",
    "opensees_exe" : "myopensees",
    "opensees_tcp_port" : 8124,
    "opensees_scripts_path" : "/"
};

function Fe_Model(cfg) {
    ut.merge(this, cfg, defaults);
    this.createdTime = new Date();
    this._createOpenSeesInterp();
    this._clients = {};

    // gmsh objects:
    this.geo_elementaries = {
        points : {},
        lines : {},
        line_loops : {},
        surfaces : {},
        surface_loops : {},
        volumns : {}
    };
    
    this.groups = {
        physical_points : {},
        physical_lines : {},
        physical_surfaces : {},
        physical_volumns : {}
    };

    // 
    this.geo_transforms = {};
    this.sections = {};
    this.uniaxial_materials = {};
    this.nd_materials = {};
    this.time_series = {};
    this.patterns = {};

    this.msh = {
        elements : {},
        nodes : {}
    };
    
}

Fe_Model.prototype._createOpenSeesInterp = function() {
    var interp = spawn(this['opensees_exe'], ['--no-prompt']);
    interp.model_id = this.id;

    // init scripts:
    // interp.stdin.write(OpenSees_initrc);
    interp.stdin.write('json-set-tcp 127.0.0.1 '
                       + this['opensees_tcp_port'] + ' '
                       + this.id + ';\n');
    
    // this.opsInterp('json-set-tcp 127.0.0.1 '
    //                + this['opensees_tcp_port'] + ' '
    //                + this.id + ';\n');
    
    this.ops_interp = interp;
};



Fe_Model.prototype.getFeInterp = function() {
    return this.ops_interp;
};

Fe_Model.prototype.listClients = function() {
    return ut.keys(this._clients);
};

Fe_Model.prototype.addClient = function(socket) {
    this._clients[socket.id] = socket;
    if (ut.isDefined(socket.model_id)) {
        socket.leave(socket.model_id);
    }
    socket.model_id = this.id;
    socket.model = this;
    socket.join(socket.model_id);
};

Fe_Model.prototype.opsInterp = function(str) {   
    function formatCommand(str) {
        var cmds=[];
        var cmd;
        if (!str) {
            return '';
        } else {
            var result = '';
            str.trim().split(/[\n;]/).forEach(function(s) {
                var cmd = s.trim().replace(/\s+/g, ' ');
                if (cmd !== '' && cmd[0] !== '#') {
                    cmds.push(cmd);
                }
            });
            for (var i = -1, len = cmds.length; ++i < len;) {
                cmd = cmds[i].trim();
                result += cmd + ';';
            }
            result += "\n";
            return result;
        }
    }
    
    if (ut.isDefined(this.ops_interp)) {
        this.ops_interp.stdin.write(formatCommand(str));
        return true;
    } else {
        return false;
    }
};

module.exports = Fe_Model;