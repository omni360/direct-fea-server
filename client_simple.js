var repl = require("repl");
var io = require('socket.io-client');
var socket = io.connect('localhost', {
    port : 3000
});


socket.on('ops-stdout', function(str) {
    console.log('ops-stdout:\n' + str);
});

socket.on('ops-stderr', function(str) {
    console.log('ops-stderr:\n' + str);
});

socket.on('ops-json-domain', function(str) {
    console.log('ops-json-domain:\n' + str);
});

var ops = function(str) {
    socket.emit('ops-stdin', str);
};

var obj1 = 3;

socket.on('connect', function() {
    console.log("socket connected");
    var obj2;
    var data = {
        x : 20,
        y : 30,
        "asdfsdf" : 'adfasdf'
    };
    socket.emit('test', data, function(err, result) {
        console.log('arguments', arguments);
        console.log('obj1:', obj1);
        obj1 = result;
        console.log('obj1 changed!!', obj1);
        r.context.obj1 = obj1;
    });

    
});

var r = repl.start({
    prompt: ">> ",
    input: process.stdin,
    output: process.stdout
});
r.context.sc = socket;
r.context.ops = ops;
